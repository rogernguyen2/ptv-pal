# Copyright 2016 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from dev import devId, key
import urllib2
from hashlib import sha1
import hmac
import binascii
import webapp2
from flask import request




class Hello(webapp2.RequestHandler):
    def get(self):
    	name = self.request.get('name')
    	self.response.headers['Content-Type'] = 'application/json'
    	self.response.write('Hello ' + name + '!')


class getRoutes(webapp2.RequestHandler):
	def get(self):
		self.response.headers['Content-Type'] = 'application/json'
		#self.response.write(getURL('/v3/routes'))
		result = urllib2.urlopen(getURL('/v3/routes'))
		self.response.write(result.read())

class getDisruptions(webapp2.RequestHandler):
	def get(self):
		route_id = self.request.get('route')
		self.response.headers['Content-Type'] = 'application/json'
		result = urllib2.urlopen(getURL('/v3/disruptions/route/'+route_id))
		self.response.write(result.read())


def getURL(request):
	request = request + ('&' if ('?' in request) else '?')
	raw = request+'devid={0}'.format(devId)
	hashed = hmac.new(key, raw, sha1)
	signature = hashed.hexdigest()
	return 'http://timetableapi.ptv.vic.gov.au'+raw+'&signature={1}'.format(devId, signature)


app = webapp2.WSGIApplication([
    ('/hello', Hello),
    ('/allroutes', getRoutes),
    ('/disruptions', getDisruptions)
], debug=True)
