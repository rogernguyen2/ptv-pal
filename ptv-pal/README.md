this guy lets you talk to PTV's API.


more info: [https://timetableapi.ptv.vic.gov.au/swagger/ui/index]

project has been built using google cloud. make sure you have gcloud SDK installed before you clone and launch via 'dev_appserver.py app.yaml' or 'gcloud app deploy'

operations are currently:

* GET - /routes - returns all route ids
* GET - /disruptions?route={route_id} - returns disruption notices based on route_id provided
